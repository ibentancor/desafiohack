/**
 * @name Cuentas
 * @version 1.0.0
 * @module
 * @description Grupo de métodos que implementan las funcionalidades con cuentas
 */

'use strict'

const bcrypt = require('bcrypt');
const saltRounds = 10;

var jwt = require('jsonwebtoken');

var requestJSON = require('request-json');
var globales = require('../globales');
var herramientas = require('./herram');

var baseMLabURL = globales.baseMLabURL;
var apikeyMLab = globales.apikeyMLab;



/** @method
 * @name crearCuenta
 * @description Método para crear una cuenta de un usuario.
 * @param {number} params.id - Id del usuario
 * @param {string} body.iban - IBAN de la cuenta
 * @param {number} body.moneda - moneda de la cuenta
 * @param {string} headers.'autorizacion' - token del usuario logueado
 * @author IBC
 * @async
 */
function crearCuenta(req, res) {
  var respuesta = {};
  var newCta = {
    "id": 0,
    "iban": req.body.iban,
    "moneda": req.body.moneda,
    "saldo": 111.11,
    "movimientos": []
  };
  var token = req.headers['autorizacion'];
  var idUsr = req.params.id;
  var queryStrField = 'f={"_id":0, "password":0, "cuentas":0}&';

  herramientas.escribirLOG("-> " + req.method + " " + req.url + " <BEGIN>");
  herramientas.escribirLOG("--> Datos nueva cuenta: " + JSON.stringify(newCta));
  if (!token || req.body.iban == undefined || req.body.moneda == undefined) {
    if (!token) {
      res.status(400).send({
        error: "Es necesario el token de autenticación."
      });
    } else {
      res.status(400).send({
        error: "Es necesario pasar todos los parámetros."
      });
    }
  } else {
    token = token.replace('Bearer ', '');
    jwt.verify(token, 'Secret Password', function(errToken, user) {
      if (!errToken && idUsr == user.id) {
        var queryStringTk = 'q={"token": "' + token + '"}&';
        var clienteMlabTk = requestJSON.createClient(baseMLabURL);
        clienteMlabTk.get('blacklisttoken?' + queryStringTk + apikeyMLab,
          function(errorTk, respuestaMLabTk, bodyTk) {
            if (!errorTk && bodyTk.length == 0) {
              var queryString = 'q={"id": ' + idUsr + '}&';
              var queryStrField = 'f={"_id":0, "cuentas":0, "password":0, "logged":0}&';
              var clienteMlab = requestJSON.createClient(baseMLabURL);
              clienteMlab.get('maestro?' + queryString + queryStrField + apikeyMLab,
                function(error, respuestaMLab, body) {
                  herramientas.escribirLOG("--> Respuesta largo body " + body.length);
                  // console.log(body);
                  if (error) {
                    respuesta = {
                      "msg": "Error obteniendo usuario."
                    };
                    res.status(500).send(respuesta);
                  } else {
                    if (body.length != undefined || body.length > 0) {
                      //     herramientas.escribirLOG("--> No existe usuario");
                      var queryString1 = 'q={"nombre":"cta"}&';
                      var consulta = 'contadores?' + queryString1 + apikeyMLab;
                      //     // console.log("--> Consulta", consulta);
                      clienteMlab.get(consulta,
                        function(error1, respuestaMLab1, body1) {
                          if (error1) {
                            herramientas.escribirLOG("--> Error al leer numeradores <END>");
                            respuesta = {
                              "msg": "Error al leer numeradores."
                            };
                            res.status(500).send(respuesta);
                          } else {
                            herramientas.escribirLOG("--> Numerador cuentas:" + body1[0].valor);
                            body1[0].valor += 1;
                            clienteMlab.put('contadores/' + body1[0]._id.$oid + '?' + apikeyMLab, body1[0],
                              function(error2, respuestaMLab2, body2) {
                                if (error2) {
                                  herramientas.escribirLOG("--> Error al grabar numeradores <END>");
                                  respuesta = {
                                    "msg": "Error al grabar numeradores."
                                  };
                                  res.status(500).send(respuesta);
                                } else {
                                  var idNva = body1[0].valor;
                                  newCta.id = idNva;
                                  // console.log(idNva);
                                  herramientas.escribirLOG("--> Cuenta dar de alta:" + JSON.stringify(newCta));
                                  var newCtaArm = '{"$push":{"cuentas":' + JSON.stringify(newCta) + '}}';
                                  // console.log(newCtaArm);
                                  //
                                  clienteMlab.put('maestro?q={"id": ' + user.id + '}&' + apikeyMLab, JSON.parse(newCtaArm),
                                    function(error3, respuestaMLab3, body3) {
                                      if (error3) {
                                        herramientas.escribirLOG("--> Error al grabar la nueva cuenta <END>");
                                        respuesta = {
                                          "msg": "Error al grabar la nueva cuenta."
                                        };
                                        res.status(500).send(respuesta);
                                      } else {
                                        respuesta = {
                                          "msg": "La cuenta ha sido creada, con $111,11 de regalo!!!"
                                        };
                                        res.status(201).send(respuesta);
                                      }
                                    });
                                }
                              });
                          }
                        });
                    } else {
                      herramientas.escribirLOG("--> Usuario NO existe <END>");
                      respuesta = {
                        "msg": "El usuario NO existe."
                      };
                      res.status(404).send(respuesta);
                    }
                  }
                });
            } else {
              herramientas.escribirLOG("--> Token inválido, blacklist <END>");
              respuesta = {
                "msg": "Token inválido."
              };
              res.status(401).send(respuesta);
            }
          });

      } else {
        herramientas.escribirLOG("--> Token inválido " + errToken + " <END>");
        respuesta = {
          "msg": "Token inválido."
        };
        res.status(401).send(respuesta);
      }
    });
  };
}; //****** crearCuenta                 ******


/** @method
 * @name listarCuentas
 * @description Método para listar las cuentas de un usuario.
 * @param {number} params.id - Id del usuario
 * @param {string} headers.'autorizacion' - token del usuario logueado
 * @author IBC
 * @async
 */
function listarCuentas(req, res) {
  var respuesta = {};
  var token = req.headers['autorizacion'];
  var idUsr = req.params.id;
  var queryStrField = 'f={"_id":0, "id":0, "first_name":0, "last_name":0, "email":0, "rol":0, "password":0, "logged":0, "cuentas.movimientos":0}&';
  var queryStrSort = 's={"cuentas.id":1}&';

  herramientas.escribirLOG("-> " + req.method + " " + req.url + " <BEGIN>");

  if (!token) {
    res.status(401).send({
      error: "Es necesario el token de autenticación"
    });
  } else {
    token = token.replace('Bearer ', '');
    jwt.verify(token, 'Secret Password', function(errToken, user) {
      if (!errToken && (idUsr == user.id || user.rol == 'admin')) {
        var queryStringTk = 'q={"token": "' + token + '"}&';
        var clienteMlabTk = requestJSON.createClient(baseMLabURL);
        clienteMlabTk.get('blacklisttoken?' + queryStringTk + apikeyMLab,
          function(errorTk, respuestaMLabTk, bodyTk) {
            if (!errorTk && bodyTk.length == 0) {
              var queryString = 'q={"id": ' + idUsr + '}&';
              var clienteMlab = requestJSON.createClient(baseMLabURL);
              clienteMlab.get('maestro?' + queryString + queryStrField + queryStrSort + apikeyMLab,
                function(error, respuestaMLab, body) {
                  herramientas.escribirLOG("--> Cantidad de cuentas: " + body[0].cuentas.length);
                  // console.log(body);
                  if (error) {
                    respuesta = {
                      "msg": "Error obteniendo cuentas."
                    };
                    res.status(500).send(respuesta);
                  } else {
                    if (body.length != undefined || body.length > 0) {
                      respuesta = body;
                      res.status(200).send(respuesta);
                    } else {
                      herramientas.escribirLOG("--> Usuario NO existe <END>");
                      respuesta = {
                        "msg": "El usuario NO existe."
                      };
                      res.status(404).send(respuesta);
                    }
                  }
                });
            } else {
              herramientas.escribirLOG("--> Token inválido, blacklist <END>");
              respuesta = {
                "msg": "Token inválido."
              };
              res.status(401).send(respuesta);
            }
          });
      } else {
        herramientas.escribirLOG("--> Token inválido " + errToken + " <END>");
        respuesta = {
          "msg": "Token inválido."
        };
        res.status(401).send(respuesta);
      }
    });
  };
}; //****** listarCuentas                 ******


/** @method
 * @name borrarCuenta
 * @summary Borrar cuenta
 * @description Método para borrar una cuenta, el usuario logueado debe ser el titular o tener rol "admin".<br>
 * La cuenta a ser borrada no puede tener movimientos.
 * @param {number} params.id - ID del usuario
 * @param {number} params.idc - ID de cuenta
 * @param {string} headers.'autorizacion' - token del usuario logueado
 * @author IBC
 * @async
 */
function borrarCuenta(req, res) {
  var clienteMlab = requestJSON.createClient(baseMLabURL);
  var idUsr = req.params.id;
  var idCta = req.params.idc;
  var token = req.headers['autorizacion'];
  var queryString = 'q={"id": ' + idUsr + ', "cuentas.id":' + idCta + '}&';
  var queryStrField = 'f={"password":0}&';
  herramientas.escribirLOG("-> " + req.method + " " + req.url + " <BEGIN>");
  herramientas.escribirLOG("--> Params: " + idUsr + " / " + idCta + " / " + token);

  if (token == undefined) {
    res.status(401).send({
      "msg": "Es necesario el token de autenticación"
    });
  } else {
    token = token.replace('Bearer ', '')
    jwt.verify(token, 'Secret Password', function(err, user) {
      if (!err && (idUsr == user.id || user.rol == 'admin')) {
        var queryStringTk = 'q={"token": "' + token + '"}&';
        var clienteMlabTk = requestJSON.createClient(baseMLabURL);
        clienteMlabTk.get('blacklisttoken?' + queryStringTk + apikeyMLab,
          function(errorTk, respuestaMLabTk, bodyTk) {
            if (!errorTk && bodyTk.length == 0) {

              clienteMlab.get('maestro?' + queryString + queryStrField + apikeyMLab, function(error, respuestaMLab, body) {
                herramientas.escribirLOG("--> Respuesta:" + JSON.stringify(body));
                var respuesta = {};
                if (error) {
                  respuesta = {
                    "msg": "Error obteniendo usuario."
                  };
                  res.status(500).send(respuesta);
                } else {
                  if (body.length > 0) {
                    herramientas.escribirLOG("--> Cantidad de cuentas: " + body[0].cuentas.length);
                    var lugarCtaEnArray = 0,
                      i = 0;
                    body[0].cuentas.forEach(function(cta) {
                      if (cta.id == idCta) lugarCtaEnArray = i;
                      i++;
                    });
                    herramientas.escribirLOG("--> Cuenta a borrar:" + body[0].cuentas[lugarCtaEnArray].id);
                    if (body[0].cuentas[lugarCtaEnArray].movimientos.length == 0 && body[0].cuentas[lugarCtaEnArray].saldo == 0) {
                      var ctaABorrar = JSON.stringify(body[0].cuentas[lugarCtaEnArray]);
                      var bajCtaArm = '{"$pull":{"cuentas":' + ctaABorrar + '}}';
                      console.log(ctaABorrar, bajCtaArm);
                      clienteMlab.put('maestro?' + queryString + apikeyMLab, JSON.parse(bajCtaArm),
                        function(error3, respuestaMLab3, body3) {
                          if (error3) {
                            herramientas.escribirLOG("--> Error al borrar cta. <END>");
                            respuesta = {
                              "msg": "Error al borrar cuenta."
                            };
                            res.status(500).send(respuesta);
                          } else {
                            herramientas.escribirLOG("--> Borra sin error" + JSON.stringify(body3) + " <END>");
                            respuesta = {
                              "msg": "Cuenta borrada correctamente."
                            };
                            res.status(200).send(respuesta);
                          }
                        });
                    } else {
                      if (body[0].cuentas[lugarCtaEnArray].movimientos.length != 0) {
                        respuesta = {
                          "msg": "La cuenta tiene movimientos, no puede borrarse."
                        };
                        res.status(401).send(respuesta);
                      } else {
                        respuesta = {
                          "msg": "El saldo de La cuenta no es cero, no puede borrarse."
                        };
                        res.status(401).send(respuesta);

                      }
                    }
                  } else {

                    respuesta = {
                      "msg": "Usuario no encontrado.",
                      body
                    };
                    res.status(404).send(respuesta);
                  }
                }
              });
            } else {
              herramientas.escribirLOG("--> Token inválido, blacklist <END>");
              respuesta = {
                "msg": "Token inválido."
              };
              res.status(401).send(respuesta);
            }
          });
      } else {
        if (err) {
          herramientas.escribirLOG("--> Token inválido <END>");
          res.status(401).send({
            "msg": 'Token inválido'
          });
        } else {
          herramientas.escribirLOG("--> Usuario no es admin <END>");
          res.status(401).send({
            "msg": 'Sin permisos para esta funcionalidad'
          });
        }
      }
    });
  }

} //****** borrarCuenta              ******

module.exports = {
  crearCuenta,
  listarCuentas,
  borrarCuenta
};
